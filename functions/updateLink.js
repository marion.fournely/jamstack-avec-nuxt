const axios = require('axios');
require('dotenv').config();
const { UPDATE_LINK } = require('./utils/linkQueries.js');
const sendQuery = require('./utils/sendQuery');
const formattedResponse = require('./utils/formattedResponse');

exports.handler = async (event) => {
    console.log("UPDATE LINK", UPDATE_LINK);
    const { _id: id, name, url, description, archived } = JSON.parse(event.body);
    const variables = { id, name, url, description, archived };
    console.log("variables", variables);

    try {
        const { updateLink: updatedLink } = await sendQuery(UPDATE_LINK, variables);
        return formattedResponse(200, updatedLink);
    } catch (e) {
        console.error(e);
        return formattedResponse(500, { err: 'Something went wrong' });
    }
}